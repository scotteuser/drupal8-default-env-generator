
generate-site:
    # use like `make generate-site name="drupal8-1"`
	make setup-project name="$(name)"
	make setup-mysql name="$(name)"
	make setup-drupal name="$(name)"
	make setup-error-log name="$(name)"
	make setup-vhost name="$(name)"
	make setup-hosts-file name="$(name)"
	sudo chown apache:www-data ../$(name)/web -R
	sudo service apache2 reload

setup-project:
	cd /var/www && composer create-project drupal-composer/drupal-project $(name) --stability dev --no-interaction

setup-mysql:
	mysql -u drupaltest -pdrupaltest -e 'CREATE DATABASE IF NOT EXISTS `$(name)` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;'

setup-drupal:
	cd /var/www/$(name)/web && drush site-install \
		--account-name=drupaltest \
		--account-mail=example@example.com \
		--account-pass=drupaltest \
		--db-url=mysql://drupaltest:drupaltest@localhost/$(name) \
		--site-name="$(name)" \
		-y

setup-error-log:
	mkdir -p /var/www/$(name)/logs
	touch /var/www/$(name)/logs/error.log

setup-vhost:
	sudo cp vhost.conf /etc/apache2/sites-available/$(name).conf
	sudo sed -i 's/REPLACETHIS/$(name)/g' /etc/apache2/sites-available/$(name).conf
	sudo a2ensite $(name).conf

setup-hosts-file:
	echo '127.0.0.1       $(name).local' | sudo tee --append /etc/hosts > /dev/null

remove-site:
	# use like `make remove-site name="drupal8-1"`
	sudo sed -i.bak '/127.0.0.1       $(name).local/d' /etc/hosts
	sudo a2dissite $(name).conf
	sudo rm /etc/apache2/sites-available/$(name).conf
	sudo service apache2 restart

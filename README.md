# Drupal 8 Default Environment Generator

## Introduction

Clone this repo on any Ubuntu machine / within any Ubuntu docker container.

## Requirements

1. drush
1. mysql
1. apache
1. drupal console

## Instructions

Run `make generate-site name="drupal8-test1"` to make a new Drupal 8 site with:

1. `/var/www/drupal8-test1` as the docroot
1. `/var/www/drupal8-test1/web` as the web root
1. `http://drupal8-test1.local` as the url
1. `drupal8-test1` as the database name

Run `make remove-site name="drupal8-test1"` to remove that Drupal site and clean up the /etc/hosts entry and vhost file.

## Notes

Perhaps obvious, but ** not for use in production or a publicly accessible url of any form **. This is insecure and has insecure passwords and is simply for rapid firing up of multiple Drupal 8 sites.

This is not meant to be a base for starting development. Best to use the composer drupal-project, an installation profile, etc. This is simply for firing up simple default installation drupal 8 sites.

## Use cases

1. Testing modules in the default Drupal 8 environment to check for and rule out conflicts.
1. Testing own and other's contrib modules and patches rapidly with easy code tweaking (ie, simplytest.me but with quick ability to edit code and retest).
